//
//  CPContainerViewController.h
//  ConciergePost
//
//  Created by Matt Hawkins on 7/25/12.
//  Copyright (c) 2012 Hawkorn LLC. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "CPHandleIntentBlock.h"
#import "CPViewControllerAnimation.h"

typedef BOOL(^CPShouldTransitionBlock)(CPIntent* intent, UIViewController* viewController);
typedef void(^CPDidTransitionBlock)(CPIntent* intent, UIViewController* viewController);

@interface CPContainerViewController : UIViewController

-(CPHandleIntentBlock)createTransitionToViewController:(Class)toViewControllerClass;
-(CPHandleIntentBlock)createTransitionToViewController:(Class)toViewControllerClass
                             withShouldTransitionBlock:(CPShouldTransitionBlock)shouldTransitionBlock
                                withDidTransitionBlock:(CPDidTransitionBlock)didTransitionBlock;

-(CPViewControllerAnimation*)getViewControllerAnimation:(enum IntentType)type;
-(void)setViewControllerAnimation:(CPViewControllerAnimation*)viewControllerAnimation forIntentType:(enum IntentType)type;

+(CPViewControllerAnimation*)createPushSlideLeft;
+(CPViewControllerAnimation*)createPushSlideRight;
+(CPViewControllerAnimation*)createSimpleAnimationWithOptions:(UIViewAnimationOptions)animationOptions;

@end
