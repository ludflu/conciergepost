//
//  CPContainerViewController.m
//  ConciergePost
//
//  Created by Matt Hawkins on 7/25/12.
//  Copyright (c) 2012 Hawkorn LLC. All rights reserved.
//

#import "CPContainerViewController.h"

@implementation CPContainerViewController {
    NSMutableDictionary* _animationOptions;
}

-(id)init {
    self = [super init];
    if (self != nil) {
        _animationOptions = [NSMutableDictionary new];
        [self setViewControllerAnimation:[CPContainerViewController createPushSlideLeft] forIntentType:INTENT_FORWARD];
        [self setViewControllerAnimation:[CPContainerViewController createPushSlideRight] forIntentType:INTENT_BACK]; 
        [self setViewControllerAnimation:[CPContainerViewController createSimpleAnimationWithOptions:UIViewAnimationOptionTransitionCurlDown] forIntentType:INTENT_MODAL_OPEN];
        [self setViewControllerAnimation:[CPContainerViewController createSimpleAnimationWithOptions:UIViewAnimationOptionTransitionCurlUp] forIntentType:INTENT_MODAL_CLOSE];
        [self setViewControllerAnimation:[CPContainerViewController createSimpleAnimationWithOptions:UIViewAnimationOptionTransitionFlipFromRight] forIntentType:INTENT_TOGGLE];
    }
    return self;
}

-(void)loadView {
    self.view = [UIView new];
    self.view.backgroundColor = [UIColor scrollViewTexturedBackgroundColor];
}

-(CPHandleIntentBlock)createTransitionToViewController:(Class)toViewControllerClass {
    return [self createTransitionToViewController:toViewControllerClass withShouldTransitionBlock:nil withDidTransitionBlock:nil];
}

-(CPHandleIntentBlock)createTransitionToViewController:(Class)toViewControllerClass
                             withShouldTransitionBlock:(CPShouldTransitionBlock)shouldTransitionBlock
                                withDidTransitionBlock:(CPDidTransitionBlock)didTransitionBlock {
    
    if (![toViewControllerClass isSubclassOfClass:[UIViewController class]]) {
        return nil;
    }
    return ^(CPIntent* intent) {
        UIViewController* toViewController = [toViewControllerClass new];
        
        // determine if we SHOULD even transition
        if (shouldTransitionBlock && !shouldTransitionBlock(intent, toViewController)) {
            return;
        }
        
        // remove old child view controllers and attach new
        NSArray* childViewControllers = [NSArray arrayWithArray:self.childViewControllers];
        for (UIViewController* vc in childViewControllers) {
            [vc willMoveToParentViewController:nil];
        }
        [self addChildViewController:toViewController];
        
        // animate to new view controller
        if (childViewControllers.count > 0) {
            UIViewController* fromViewController = childViewControllers[0];
            
            CPViewControllerAnimation* viewControllerAnimation = [self getViewControllerAnimation:intent.type];
            
            if (viewControllerAnimation.willAnimateBlock) {
                viewControllerAnimation.willAnimateBlock(self, fromViewController, toViewController);
            }
            
            [self transitionFromViewController:fromViewController
                              toViewController:toViewController
                                      duration:0.5
                                       options:viewControllerAnimation.animationOptions
                                    animations:^{
                                        if (viewControllerAnimation.didAnimateBlock) {
                                            viewControllerAnimation.didAnimateBlock(self, fromViewController, toViewController);
                                        }
                                    }
                                    completion:^(BOOL finished) {
                                        for (UIViewController* vc in childViewControllers) {
                                            [vc removeFromParentViewController];
                                        }
                                        [toViewController didMoveToParentViewController:self];
                                        if (didTransitionBlock) {
                                            didTransitionBlock(intent, toViewController);
                                        }
                                    }];
        } else {
            // first controller on stack
            [self addChildViewController:toViewController];
            [self.view addSubview:toViewController.view];
            [toViewController didMoveToParentViewController:self];
            if (didTransitionBlock) {
                didTransitionBlock(intent, toViewController);
            }
        }
    };
}

-(CPViewControllerAnimation*)getViewControllerAnimation:(enum IntentType)type {
    NSNumber* typeAsNumber = [NSNumber numberWithInt:type];
    return (CPViewControllerAnimation*)_animationOptions[typeAsNumber];
}

-(void)setViewControllerAnimation:(CPViewControllerAnimation*)viewControllerAnimation forIntentType:(enum IntentType)type {
    NSNumber* typeAsNumber = [NSNumber numberWithInt:type];
    _animationOptions[typeAsNumber] = viewControllerAnimation;
}

+(CPViewControllerAnimation*)createPushSlideLeft {
    return [[CPViewControllerAnimation alloc]
            initWithAnimationOptions:UIViewAnimationOptionTransitionNone
            andWillAnimateBlock:^(UIViewController* containerController, UIViewController* fromController, UIViewController* toController) {
                toController.view.frame = CGRectMake(containerController.view.bounds.size.width, 0,
                                                     containerController.view.bounds.size.width, containerController.view.bounds.size.height);
            }
            andDidAnimateBlock:^(UIViewController* containerController, UIViewController* fromController, UIViewController* toController) {
                fromController.view.frame = CGRectMake(-containerController.view.bounds.size.width, 0,
                                                       containerController.view.bounds.size.width, containerController.view.bounds.size.height);
                toController.view.frame = CGRectMake(0, 0,
                                                     containerController.view.bounds.size.width, containerController.view.bounds.size.height);
            }];
}

+(CPViewControllerAnimation*)createPushSlideRight {
    return [[CPViewControllerAnimation alloc]
            initWithAnimationOptions:UIViewAnimationOptionTransitionNone
            andWillAnimateBlock:^(UIViewController* containerController, UIViewController* fromController, UIViewController* toController) {
                toController.view.frame = CGRectMake(-containerController.view.bounds.size.width, 0,
                                                     containerController.view.bounds.size.width, containerController.view.bounds.size.height);
                
            }
            andDidAnimateBlock:^(UIViewController* containerController, UIViewController* fromController, UIViewController* toController) {
                fromController.view.frame = CGRectMake(containerController.view.bounds.size.width, 0,
                                                       containerController.view.bounds.size.width, containerController.view.bounds.size.height);
                toController.view.frame = CGRectMake(0, 0,
                                                     containerController.view.bounds.size.width, containerController.view.bounds.size.height);
            }];
}

+(CPViewControllerAnimation*)createSimpleAnimationWithOptions:(UIViewAnimationOptions)animationOptions {
    return [[CPViewControllerAnimation alloc] initWithAnimationOptions:animationOptions andWillAnimateBlock:nil andDidAnimateBlock:nil];
}

@end
