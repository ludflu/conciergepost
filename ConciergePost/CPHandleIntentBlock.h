//
//  CPIntentHandler.h
//  ConciergePost
//
//  Created by Matt Hawkins on 7/23/12.
//  Copyright (c) 2012 Hawkorn LLC. All rights reserved.
//

#import "CPIntent.h"

typedef void(^CPHandleIntentBlock)(CPIntent* intent);
