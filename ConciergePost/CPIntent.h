//
//  CPIntent.h
//  ConciergePost
//
//  Created by Matt Hawkins on 7/23/12.
//  Copyright (c) 2012 Hawkorn LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

NS_ENUM(int, IntentType) {
    INTENT_FORWARD,
    INTENT_BACK,
    INTENT_MODAL_OPEN,
    INTENT_MODAL_CLOSE,
    INTENT_TOGGLE,
};

@interface CPIntent : NSObject <NSCopying>

@property (strong, nonatomic) NSString* path;
@property (strong, nonatomic) NSDictionary* params;
@property (assign, nonatomic) enum IntentType type;

-(id)initWithPath:(NSString*)path;
-(id)initWithPath:(NSString*)path andParams:(NSDictionary*)params;
-(id)initWithPath:(NSString*)path andType:(enum IntentType)type;
-(id)initWithPath:(NSString*)path andParams:(NSDictionary*)params andType:(enum IntentType)type;
-(NSArray*)pathAsArray;

+(CPIntent*)intentForURL:(NSURL*)url;
+(NSDictionary*)paramsForQuery:(NSString*)query;

@end
