//
//  CPIntent.m
//  ConciergePost
//
//  Created by Matt Hawkins on 7/23/12.
//  Copyright (c) 2012 Hawkorn LLC. All rights reserved.
//

#import "CPIntent.h"

@implementation CPIntent

-(id)init {
    return [self initWithPath:@""];
}

-(id)initWithPath:(NSString*)path {
    return [self initWithPath:path andParams:nil];
}

-(id)initWithPath:(NSString*)path andParams:(NSDictionary*)params {
    return [self initWithPath:path andParams:params andType:INTENT_FORWARD];
}

-(id)initWithPath:(NSString*)path andType:(enum IntentType)type {
    return [self initWithPath:path andParams:nil andType:type];
}

-(id)initWithPath:(NSString*)path andParams:(NSDictionary*)params andType:(enum IntentType)type {
    self = [super init];
    if (self != nil) {
        self.path = path;
        self.params = params;
        self.type = type;
    }
    return self;
}

-(void)setPath:(NSString *)path {
    NSString* tempPath = path;
    if ([tempPath hasPrefix:@"/"]) {
        // ensure there isn't a leading slash... path parts are all valid spots
        NSRegularExpression* removeLeadingSlashesRegex = [[NSRegularExpression alloc] initWithPattern:@"^/+" options:0 error:nil];
        tempPath = [removeLeadingSlashesRegex stringByReplacingMatchesInString:tempPath
                                                                       options:0
                                                                         range:NSMakeRange(0, tempPath.length)
                                                                  withTemplate:@""];
    }
    
    _path = tempPath;
}

-(NSString*)description {
    return [NSString stringWithFormat:@"%@%@", self.path, [self paramsAsString]];
}

-(NSArray*)pathAsArray {    
    return [self.path componentsSeparatedByString:@"/"];
}

-(NSString*)paramsAsString {
    if (self.params.count <= 0) return @"";
    
    NSMutableString* paramString = [@"" mutableCopy];
    
    for (NSString* key in self.params) {
        [paramString appendFormat:@"&%@=%@", key, self.params[key]];
    }
    
    return [@"?" stringByAppendingString:[paramString substringFromIndex:1]];
}

-(id)copyWithZone:(NSZone *)zone {
    CPIntent* newIntent = [[CPIntent allocWithZone:zone] init];
    newIntent.path = [NSString stringWithString:self.path];
    newIntent.type = self.type;
    if (self.params) {
        NSMutableDictionary* newParams = [NSMutableDictionary new];
        for (NSString* key in self.params) {
            newParams[key] = [NSString stringWithString:self.params[key]];
        }
        newIntent.params = newParams;
    }
    return newIntent;
}

+(CPIntent*)intentForURL:(NSURL *)url {
    return [[CPIntent alloc] initWithPath:url.path andParams:[CPIntent paramsForQuery:url.query]];
}

+(NSDictionary*)paramsForQuery:(NSString*)query {
    NSMutableDictionary* params = [NSMutableDictionary dictionary];
    for (NSString* paramString in [query componentsSeparatedByString:@"&"]) {
        NSArray* paramParts = [paramString componentsSeparatedByString:@"="];
        NSString* key = [paramParts[0] stringByReplacingPercentEscapesUsingEncoding:NSASCIIStringEncoding];
        NSString* value = (paramParts.count > 1 ?
                           [paramParts[1] stringByReplacingPercentEscapesUsingEncoding:NSASCIIStringEncoding] :
                           @"");
        params[key] = value;
    }
    
    return params;
}

@end
