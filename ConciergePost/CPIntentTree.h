//
//  CPIntentTree.h
//  ConciergePost
//
//  Created by Matt Hawkins on 7/23/12.
//  Copyright (c) 2012 Hawkorn LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

#import "CPHandleIntentBlock.h"

@interface CPIntentTree : NSObject

@property (strong, nonatomic) NSMutableDictionary* children;
@property (strong, nonatomic) NSMutableDictionary* paramChildren;
@property (strong, nonatomic) CPHandleIntentBlock handleIntentBlock;

-(CPIntentTree*)findIntentTreeByIntent:(CPIntent*)intent;
-(BOOL)unregisterHandleIntentBlock:(CPHandleIntentBlock)handleIntentBlock;

@end
