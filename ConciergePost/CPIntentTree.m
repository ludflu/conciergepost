//
//  CPIntentTree.m
//  ConciergePost
//
//  Created by Matt Hawkins on 7/23/12.
//  Copyright (c) 2012 Hawkorn LLC. All rights reserved.
//

#import "CPIntentTree.h"
#import "NSDictionary+Params.h"

@implementation CPIntentTree

-(id)init {
    self = [super init];
    if (self != nil) {
        self.children = [NSMutableDictionary new];
        self.paramChildren = [NSMutableDictionary new];
    }
    return self;
}

-(NSString*)description {
    NSString* desc = [self.handleIntentBlock description] ? @"handled" : @"--";
    
    for (NSString* child in self.children) {
        desc = [desc stringByAppendingFormat:@"\n%@ %@", child,
                [[self.children[child] description] stringByReplacingOccurrencesOfString:@"\n" withString:@"\n "]];
    }
    
    for (NSDictionary* paramChild in self.paramChildren) {
        NSString* paramString = [paramChild paramDescription];
        desc = [desc stringByAppendingFormat:@"\n%@ %@", paramString,
                [[self.paramChildren[paramChild] description] stringByReplacingOccurrencesOfString:@"\n" withString:@"\n "]];
    }
    
    return desc;
}

-(CPIntentTree*)findIntentTreeByIntent:(CPIntent*)intent {
    return [self findIntentTreeByArray:[intent pathAsArray] withParams:intent.params];
}

-(CPIntentTree*)findIntentTreeByArray:(NSArray*)path withParams:(NSDictionary*)params {
    CPIntentTree* bestIntentTree = nil;
    
    // base case if we find the right node
    if (path && path.count > 0) {
        // try to navigate deeper for a "better fit"
        NSMutableArray* newPath = [path mutableCopy];
        [newPath removeObjectAtIndex:0];
        
        CPIntentTree* subTree = self.children[path[0]];
        if (subTree != nil) {
            bestIntentTree = [subTree findIntentTreeByArray:newPath withParams:params];
        }
    }
    
    if (!bestIntentTree) {
        // no deeper entities, try param children of this node
        NSUInteger bestScore = 0;
        for (NSDictionary* paramChild in self.paramChildren) {
            // paramChild is a better match if it has all of its requirements met and handles an intent
            CPIntentTree* paramChildIntentTree = (CPIntentTree*)self.paramChildren[paramChild];
            if (!paramChildIntentTree || !paramChildIntentTree.handleIntentBlock || ![paramChild isParamSubsetOf:params]) continue;
            
            // now score the number of matches... this is equal to number of keys and values matched
            NSUInteger score = paramChild.count;
            for (NSString* value in paramChild.allValues) {
                if (value.length > 0) score++;
            }
            
            if (score > bestScore) {
                bestScore = score;
                bestIntentTree = paramChildIntentTree;
            }
        }
    }
    
    if (!bestIntentTree) {
        // still no match, try self
        if (self.handleIntentBlock) bestIntentTree = self;
    }
    
    return bestIntentTree;
}

-(BOOL)unregisterHandleIntentBlock:(CPHandleIntentBlock)handleIntentBlock {
    BOOL removed = NO;
    
    if (self.handleIntentBlock == handleIntentBlock) {
        self.handleIntentBlock = nil;
        removed = YES;;
    }
    
    for (NSString* subtreePath in self.children) {
        removed = ([self.children[subtreePath] unregisterHandleIntentBlock:handleIntentBlock] || removed);
    }
    
    return removed;
}

@end
