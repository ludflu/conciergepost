//
//  CPNavigations.h
//  ConciergePost
//
//  Created by Matt Hawkins on 7/24/12.
//  Copyright (c) 2012 Hawkorn LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface CPNavigations : NSObject

@property (strong, nonatomic, readonly) NSArray* navigableClasses;

-(void)refreshNavigableClasses;

@end
