//
//  CPNavigations.m
//  ConciergePost
//
//  Created by Matt Hawkins on 7/24/12.
//  Copyright (c) 2012 Hawkorn LLC. All rights reserved.
//

#import "CPNavigations.h"
#import "CPNavigable.h"
#import <objc/runtime.h>

@implementation CPNavigations

-(id)init {
    self = [super init];
    if (self != nil) {
        [self refreshNavigableClasses];
    }
    return self;
}

-(NSArray*)findNavigableClasses {
    NSMutableArray* navigableClasses = [NSMutableArray new];
    
    Class* classes = NULL;
    int numClasses = objc_getClassList(NULL, 0);
    
    if (numClasses > 0) {
        classes = (__unsafe_unretained Class *)malloc(sizeof(Class) * numClasses);
        numClasses = objc_getClassList(classes, numClasses);
        for (int i = 0; i < numClasses; i++) {
            Class nextClass = classes[i];
            if (class_conformsToProtocol(nextClass, @protocol(CPNavigable))) {
                [navigableClasses addObject:nextClass];
            }
        }
        free(classes);
    }
    
    return [NSArray arrayWithArray:navigableClasses];
}

-(void)refreshNavigableClasses {
    _navigableClasses = [self findNavigableClasses];
}

@end
