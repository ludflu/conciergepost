//
//  CPViewControllerAnimation.h
//  ConciergePost
//
//  Created by Matt Hawkins on 7/31/12.
//  Copyright (c) 2012 Hawkorn LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>

typedef void (^CPViewControllerWillAnimateBlock)(UIViewController* containerController, UIViewController* fromController, UIViewController* toController);
typedef void (^CPViewControllerDidAnimateBlock)(UIViewController* containerController, UIViewController* fromController, UIViewController* toController);

@interface CPViewControllerAnimation : NSObject

@property (assign, nonatomic) UIViewAnimationOptions animationOptions;
@property (assign, nonatomic) CPViewControllerWillAnimateBlock willAnimateBlock;
@property (assign, nonatomic) CPViewControllerDidAnimateBlock didAnimateBlock;

-(id)initWithAnimationOptions:(UIViewAnimationOptions)animationOptions
          andWillAnimateBlock:(CPViewControllerWillAnimateBlock)willAnimateBlock
           andDidAnimateBlock:(CPViewControllerDidAnimateBlock)didAnimateBlock;

@end
