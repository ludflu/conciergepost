//
//  CPViewControllerAnimation.m
//  ConciergePost
//
//  Created by Matt Hawkins on 7/31/12.
//  Copyright (c) 2012 Hawkorn LLC. All rights reserved.
//

#import "CPViewControllerAnimation.h"

@implementation CPViewControllerAnimation


-(id)initWithAnimationOptions:(UIViewAnimationOptions)animationOptions
          andWillAnimateBlock:(CPViewControllerWillAnimateBlock)willAnimateBlock
           andDidAnimateBlock:(CPViewControllerDidAnimateBlock)didAnimateBlock {
    self = [self init];
    if (self != nil) {
        self.animationOptions = animationOptions;
        self.willAnimateBlock = willAnimateBlock;
        self.didAnimateBlock = didAnimateBlock;
    }
    
    return self;
}

@end
