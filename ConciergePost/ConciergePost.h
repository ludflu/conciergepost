//
//  ConciergePost.h
//  ConciergePost
//
//  Created by Matt Hawkins on 7/23/12.
//  Copyright (c) 2012 Hawkorn LLC. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CPIntent.h"
#import "CPHandleIntentBlock.h"
#import "CPIntentTree.h"

@interface ConciergePost : NSObject

@property (strong, nonatomic, readonly) CPIntentTree* intentTree;
@property (strong, nonatomic) NSMutableArray* history;
@property (strong, nonatomic) NSMutableArray* stack;

#pragma mark - Intent Setup and Organization
-(BOOL)unregisterHandleIntentBlock:(CPHandleIntentBlock)handleIntentBlock;
-(BOOL)unregisterHandleIntentBlock:(CPHandleIntentBlock)handleIntentBlock forIntent:(CPIntent*)intent;
-(BOOL)unregisterIntent:(CPIntent*)intent andChildren:(BOOL)removeChildren;
-(BOOL)registerHandleIntentBlock:(CPHandleIntentBlock)handleIntentBlock forIntent:(CPIntent*)intent;

#pragma mark - Intent Execution
-(BOOL)handleIntent:(CPIntent*)intent;
-(CPIntent*)popIntentAndExecute:(BOOL)execute; // returns popped intent, and possibly executes the next on stack

@end
