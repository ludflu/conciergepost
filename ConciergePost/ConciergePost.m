//
//  ConciergePost.m
//  ConciergePost
//
//  Created by Matt Hawkins on 7/23/12.
//  Copyright (c) 2012 Hawkorn LLC. All rights reserved.
//

#import "ConciergePost.h"
#import "CPNavigations.h"
#import "NSDictionary+Params.h"

@implementation ConciergePost {
    CPNavigations* _navigations;
}

-(id)init {
    self = [super init];
    if (self != nil) {
        _intentTree = [CPIntentTree new];
        
        self.history = [NSMutableArray new];
        self.stack = [NSMutableArray new];
        
        _navigations = [CPNavigations new];
    }
    return self;
}

-(BOOL)unregisterHandleIntentBlock:(CPHandleIntentBlock)handleIntentBlock {
    return [self.intentTree unregisterHandleIntentBlock:handleIntentBlock];
}

-(BOOL)unregisterHandleIntentBlock:(CPHandleIntentBlock)handleIntentBlock forIntent:(CPIntent*)intent {
    CPIntentTree* intentTree = [self.intentTree findIntentTreeByIntent:intent];
    if (!intentTree || !intentTree.handleIntentBlock) return NO;
        
    intentTree.handleIntentBlock = nil;
    return YES;
}

-(BOOL)unregisterIntent:(CPIntent*)intent andChildren:(BOOL)removeChildren {
    CPIntentTree* intentTree = [self.intentTree findIntentTreeByIntent:intent];
    if (!intentTree || !intentTree.handleIntentBlock) return NO;
    
    intentTree.handleIntentBlock = nil;
    if (removeChildren) {
        intentTree.children = [NSMutableDictionary new];
    }
    return YES;
}

-(BOOL)registerHandleIntentBlock:(CPHandleIntentBlock)handleIntentBlock forIntent:(CPIntent*)intent {
    CPIntentTree* currentTree = self.intentTree;
    
    // find the right tree location for path, creating nodes as required drilling down
    NSMutableArray* pathParts = [[intent pathAsArray] mutableCopy];
    while (pathParts.count > 0) {
        NSString* match = pathParts[0];
        [pathParts removeObjectAtIndex:0];
        
        if (!currentTree.children[match]) {
            currentTree.children[match] = [CPIntentTree new];
        }
        currentTree = (CPIntentTree*)currentTree.children[match];
    }
    // current tree points to correct one based on path alone, now align based on parameters
    if (intent.params && intent.params.count > 0) {
        BOOL paramMatchFound = NO;
        for (NSDictionary* paramChild in currentTree.paramChildren) {
            if (paramChild.count <= 0) continue;
            
            if ([paramChild isParamEquivalent:intent.params]) {
                // this tree matches the required parameters
                paramMatchFound = YES;
                currentTree = (CPIntentTree*)currentTree.paramChildren[paramChild];
                break;
            }
        }
        
        if (!paramMatchFound) {
            // add new param child for these parameters
            currentTree.paramChildren[intent.params] = [CPIntentTree new];
            currentTree = currentTree.paramChildren[intent.params];
        }
    }
    
    // current tree now exists and holds the correct location to handle the intent
    currentTree.handleIntentBlock = handleIntentBlock;
    
    return YES;
}

-(BOOL)handleIntent:(CPIntent*)intent {
    return [self handleIntent:intent andStack:YES];
}

-(BOOL)handleIntent:(CPIntent*)intent andStack:(BOOL)stack {
    CPIntentTree* intentTree = [self.intentTree findIntentTreeByIntent:intent];
    if (!intentTree || !intentTree.handleIntentBlock) {
        NSLog(@"Unhandled Intent: %@", [intent description]);
        return NO;
    }
    
    
    [self.history addObject:intent];
    if (stack) {
        [self.stack addObject:intent];
    }
    intentTree.handleIntentBlock(intent);
    
    return YES;
}

-(CPIntent*)popIntentAndExecute:(BOOL)execute {
    if (self.stack.count <= 0) return nil;
    
    CPIntent* poppedIntent = (CPIntent*)[self.stack lastObject];
    [self.stack removeLastObject];
    
    if (execute && self.stack.count > 0) {
        CPIntent* inversePoppedIntent = [(CPIntent*)[self.stack lastObject] copy];
        inversePoppedIntent.type = poppedIntent.type;
        // swap direction of last intent navigation for animations to be logical
        switch (inversePoppedIntent.type) {
            case INTENT_FORWARD: inversePoppedIntent.type = INTENT_BACK; break;
            case INTENT_MODAL_OPEN: inversePoppedIntent.type = INTENT_MODAL_CLOSE; break;
            default: break;
        }
        [self handleIntent:inversePoppedIntent andStack:NO];
    }
    
    return poppedIntent;
}

@end
