//
//  NSDictionary+Subset.h
//  ConciergePost
//
//  Created by Matt Hawkins on 7/31/12.
//  Copyright (c) 2012 Hawkorn LLC. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface NSDictionary (Params)

-(BOOL)isParamEquivalent:(NSDictionary*)dictionary;
-(BOOL)isParamSubsetOf:(NSDictionary*)dictionary;
-(NSString*)paramDescription;
    
@end
