//
//  NSDictionary+Subset.m
//  ConciergePost
//
//  Created by Matt Hawkins on 7/31/12.
//  Copyright (c) 2012 Hawkorn LLC. All rights reserved.
//

#import "NSDictionary+Params.h"

@implementation NSDictionary (Params)

-(BOOL)isParamEquivalent:(NSDictionary*)dictionary {
    return [self isParamSubsetOf:dictionary] && [dictionary isParamSubsetOf:self];
}

-(BOOL)isParamSubsetOf:(NSDictionary*)dictionary {
    // can't be a subset if it has more elements
    if (self.count > dictionary.count) return NO;
    
    for (NSString* key in self) {
        // a isn't in b, then A is not a subset of B
        if (!dictionary[key]) return NO;
        
        // if a is in both and a has a value, then b must have the same value
        NSString* value = (NSString*)self[key];
        if (value.length > 0 && ![value isEqualToString:dictionary[key]]) return NO;
    }
    
    return YES;
}

-(NSString*)paramDescription {
    NSMutableString* paramString = [@"" mutableCopy];
    
    if (self.count > 0) [paramString appendString:@"?"];
    
    for (NSString* key in self) {
        [paramString appendString:key];
        if ([self[key] length] > 0) {
            [paramString appendFormat:@"=%@", self[key]];
        }
    }
    
    return paramString;
}

@end
