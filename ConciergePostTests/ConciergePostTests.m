//
//  ConciergePostTests.m
//  ConciergePostTests
//
//  Created by Matt Hawkins on 7/23/12.
//  Copyright (c) 2012 Hawkorn LLC. All rights reserved.
//

#import "ConciergePostTests.h"

@implementation ConciergePostTests

- (void)setUp
{
    [super setUp];
}

- (void)tearDown
{
    [super tearDown];
}

-(void)testAllocation {
    ConciergePost* conciergePost = [ConciergePost new];
    STAssertNotNil(conciergePost, @"Could not allocate ConciergePost");
}

-(void)testRegistration {
    ConciergePost* conciergePost = [ConciergePost new];
    
    CPHandleIntentBlock handleIntentBlock = ^(CPIntent* intent) {
    };
    
    STAssertEquals([conciergePost.intentTree.children count], 0U, @"Intent tree should initialize empty");
    CPIntent* viewIntent = [[CPIntent alloc] initWithPath:@"view"];
    [conciergePost registerHandleIntentBlock:handleIntentBlock forIntent:viewIntent];
    STAssertNotNil(conciergePost.intentTree.children[@"view"], @"Registration of first intent failed");
    CPIntent* editIntent = [[CPIntent alloc] initWithPath:@"edit"];
    [conciergePost registerHandleIntentBlock:handleIntentBlock forIntent:editIntent];
    STAssertNotNil(conciergePost.intentTree.children[@"edit"], @"Registration of second intent failed");
    CPIntent* viewObjectIntent = [[CPIntent alloc] initWithPath:@"view/object"];
    [conciergePost registerHandleIntentBlock:handleIntentBlock forIntent:viewObjectIntent];
    STAssertNotNil(((CPIntentTree*)conciergePost.intentTree.children[@"view"]).children[@"object"], @"Registration of deep intent failed");
    STAssertNil(((CPIntentTree*)conciergePost.intentTree.children[@"view"]).children[@"object2"], @"Extra intent was found");
}

-(void)testUnregisterHandleIntentBlockForIntent {
    ConciergePost* conciergePost = [ConciergePost new];
    
    CPHandleIntentBlock handleIntentBlock = ^(CPIntent* intent) {
    };
    CPIntent* viewIntent = [[CPIntent alloc] initWithPath:@"view"];
    [conciergePost registerHandleIntentBlock:handleIntentBlock forIntent:viewIntent];
    CPIntent* editIntent = [[CPIntent alloc] initWithPath:@"edit"];
    [conciergePost registerHandleIntentBlock:handleIntentBlock forIntent:editIntent];
    CPIntent* viewObjectIntent = [[CPIntent alloc] initWithPath:@"view/object"];
    [conciergePost registerHandleIntentBlock:handleIntentBlock forIntent:viewObjectIntent];
    
    [conciergePost unregisterHandleIntentBlock:handleIntentBlock forIntent:viewIntent];
    STAssertNil([conciergePost.intentTree findIntentTreeByIntent:viewIntent].handleIntentBlock, nil);
    STAssertNotNil([conciergePost.intentTree findIntentTreeByIntent:editIntent].handleIntentBlock, nil);
    STAssertNotNil([conciergePost.intentTree findIntentTreeByIntent:viewObjectIntent].handleIntentBlock, nil);
}

-(void)testUnregisterHandleIntentBlock {
    ConciergePost* conciergePost = [ConciergePost new];
    
    CPHandleIntentBlock handleIntentBlock = ^(CPIntent* intent) {
    };
    CPIntent* viewIntent = [[CPIntent alloc] initWithPath:@"view"];
    [conciergePost registerHandleIntentBlock:handleIntentBlock forIntent:viewIntent];
    CPIntent* editIntent = [[CPIntent alloc] initWithPath:@"edit"];
    [conciergePost registerHandleIntentBlock:handleIntentBlock forIntent:editIntent];
    CPIntent* viewObjectIntent = [[CPIntent alloc] initWithPath:@"view/object"];
    [conciergePost registerHandleIntentBlock:handleIntentBlock forIntent:viewObjectIntent];
    
    [conciergePost unregisterHandleIntentBlock:handleIntentBlock];
    STAssertNil([conciergePost.intentTree findIntentTreeByIntent:viewIntent].handleIntentBlock, nil);
    STAssertNil([conciergePost.intentTree findIntentTreeByIntent:editIntent].handleIntentBlock, nil);
    STAssertNil([conciergePost.intentTree findIntentTreeByIntent:viewObjectIntent].handleIntentBlock, nil);
}

-(void)testUnregisterIntent {
    ConciergePost* conciergePost = [ConciergePost new];
    
    CPHandleIntentBlock handleIntentBlock = ^(CPIntent* intent) {
    };
    CPIntent* viewIntent = [[CPIntent alloc] initWithPath:@"view"];
    [conciergePost registerHandleIntentBlock:handleIntentBlock forIntent:viewIntent];
    CPIntent* editIntent = [[CPIntent alloc] initWithPath:@"edit"];
    [conciergePost registerHandleIntentBlock:handleIntentBlock forIntent:editIntent];
    CPIntent* viewObjectIntent = [[CPIntent alloc] initWithPath:@"view/object"];
    [conciergePost registerHandleIntentBlock:handleIntentBlock forIntent:viewObjectIntent];
    
    [conciergePost unregisterIntent:viewIntent andChildren:NO];
    STAssertNil([conciergePost.intentTree findIntentTreeByIntent:viewIntent].handleIntentBlock, nil);
    STAssertNotNil([conciergePost.intentTree findIntentTreeByIntent:editIntent].handleIntentBlock, nil);
    STAssertNotNil([conciergePost.intentTree findIntentTreeByIntent:viewObjectIntent].handleIntentBlock, nil);
}

-(void)testUnregisterIntentAndChildren {
    ConciergePost* conciergePost = [ConciergePost new];
    
    CPHandleIntentBlock handleIntentBlock = ^(CPIntent* intent) {
    };
    CPIntent* viewIntent = [[CPIntent alloc] initWithPath:@"view"];
    [conciergePost registerHandleIntentBlock:handleIntentBlock forIntent:viewIntent];
    CPIntent* editIntent = [[CPIntent alloc] initWithPath:@"edit"];
    [conciergePost registerHandleIntentBlock:handleIntentBlock forIntent:editIntent];
    CPIntent* viewObjectIntent = [[CPIntent alloc] initWithPath:@"view/object"];
    [conciergePost registerHandleIntentBlock:handleIntentBlock forIntent:viewObjectIntent];
    
    [conciergePost unregisterIntent:viewIntent andChildren:YES];
    STAssertNil([conciergePost.intentTree findIntentTreeByIntent:viewIntent].handleIntentBlock, nil);
    STAssertNotNil([conciergePost.intentTree findIntentTreeByIntent:editIntent].handleIntentBlock, nil);
    STAssertNil([conciergePost.intentTree findIntentTreeByIntent:viewObjectIntent].handleIntentBlock, nil);
}

-(void)testHandleIntent {
    ConciergePost* conciergePost = [ConciergePost new];
    
    __block BOOL viewCalled = NO;
    __block BOOL editCalled = NO;
    __block BOOL viewObjectCalled = NO;
    
    CPIntent* viewIntent = [[CPIntent alloc] initWithPath:@"view"];
    [conciergePost registerHandleIntentBlock:^(CPIntent* intent) {
        viewCalled = YES;
    } forIntent:viewIntent];
    CPIntent* editIntent = [[CPIntent alloc] initWithPath:@"edit"];
    [conciergePost registerHandleIntentBlock:^(CPIntent* intent) {
        editCalled = YES;
    } forIntent:editIntent];
    CPIntent* viewObjectIntent = [[CPIntent alloc] initWithPath:@"view/object"];
    [conciergePost registerHandleIntentBlock:^(CPIntent* intent) {
        viewObjectCalled = YES;
    } forIntent:viewObjectIntent];
    
    [conciergePost handleIntent:editIntent];
    STAssertTrue(editCalled, nil);
    [conciergePost handleIntent:viewObjectIntent];
    STAssertTrue(viewObjectCalled, nil);
    STAssertFalse(viewCalled, nil);
}

-(void)testHistory {
    ConciergePost* conciergePost = [ConciergePost new];
    
    CPHandleIntentBlock handleIntentBlock = ^(CPIntent* intent) {
    };
    CPIntent* viewIntent = [[CPIntent alloc] initWithPath:@"view"];
    [conciergePost registerHandleIntentBlock:handleIntentBlock forIntent:viewIntent];
    CPIntent* editIntent = [[CPIntent alloc] initWithPath:@"edit"];
    [conciergePost registerHandleIntentBlock:handleIntentBlock forIntent:editIntent];
    CPIntent* viewObjectIntent = [[CPIntent alloc] initWithPath:@"view/object"];
    [conciergePost registerHandleIntentBlock:handleIntentBlock forIntent:viewObjectIntent];
    
    STAssertEquals(conciergePost.history.count, 0U, nil);
    [conciergePost handleIntent:editIntent];
    STAssertEquals(conciergePost.history.count, 1U, nil);
    [conciergePost handleIntent:viewObjectIntent];
    STAssertEquals(conciergePost.history.count, 2U, nil);
}

-(void)testStack {
    ConciergePost* conciergePost = [ConciergePost new];
    
    __block int viewCallCount = 0;
    __block int viewObjectCallCount = 0;
    __block int editCallCount = 0;
    CPIntent* viewIntent = [[CPIntent alloc] initWithPath:@"view"];
    [conciergePost registerHandleIntentBlock:^(CPIntent* intent) {
        viewCallCount++;
    } forIntent:viewIntent];
    CPIntent* editIntent = [[CPIntent alloc] initWithPath:@"edit"];
    [conciergePost registerHandleIntentBlock:^(CPIntent* intent) {
        editCallCount++;
    } forIntent:editIntent];
    CPIntent* viewObjectIntent = [[CPIntent alloc] initWithPath:@"view/object"];
    [conciergePost registerHandleIntentBlock:^(CPIntent* intent) {
        viewObjectCallCount++;
    } forIntent:viewObjectIntent];
    
    CPIntent* poppedIntent = nil;
    
    STAssertEquals(conciergePost.stack.count, 0U, nil);
    [conciergePost handleIntent:viewObjectIntent];
    STAssertEquals(conciergePost.stack.count, 1U, nil);
    [conciergePost handleIntent:editIntent];
    STAssertEquals(conciergePost.stack.count, 2U, nil);
    [conciergePost handleIntent:viewIntent];
    STAssertEquals(conciergePost.stack.count, 3U, nil);
    poppedIntent = [conciergePost popIntentAndExecute:NO];
    STAssertEquals(conciergePost.stack.count, 2U, nil);
    STAssertEquals(poppedIntent, viewIntent, nil);
    STAssertEquals(viewCallCount, 1, nil);
    STAssertEquals(editCallCount, 1, nil);
    poppedIntent = [conciergePost popIntentAndExecute:YES];
    STAssertEquals(conciergePost.stack.count, 1U, nil);
    STAssertEquals(poppedIntent, editIntent, nil);
    STAssertEquals(editCallCount, 1, nil);
    STAssertEquals(viewObjectCallCount, 2, nil); // should have been executed during previous pop
    STAssertEquals(viewCallCount, 1, nil);
}

@end
