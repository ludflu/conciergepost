//
//  NSDictionary+Params.m
//  ConciergePost
//
//  Created by Matt Hawkins on 8/1/12.
//  Copyright (c) 2012 Hawkorn LLC. All rights reserved.
//

#import "NSDictionary+ParamsTests.h"
#import "NSDictionary+Params.h"

@implementation NSDictionary_ParamsTests

-(void)testParamSubsets {
    NSDictionary* A = @{};
    NSDictionary* B = @{@"1": @""};
    NSDictionary* C = @{@"1": @"", @"2": @""};
    NSDictionary* D = @{@"1": @"yes", @"2": @""};
    NSDictionary* E = @{@"1": @"no", @"2": @""};
    NSDictionary* F = @{@"1": @"yes", @"2": @"no"};
    
    STAssertTrue([A isParamSubsetOf:A], nil);
    STAssertTrue([A isParamSubsetOf:B], nil);
    STAssertFalse([B isParamSubsetOf:A], nil);
    STAssertTrue([A isParamSubsetOf:C], nil);
    STAssertTrue([B isParamSubsetOf:C], nil);
    STAssertTrue([C isParamSubsetOf:D], nil);
    STAssertFalse([D isParamSubsetOf:E], nil);
    STAssertFalse([E isParamSubsetOf:D], nil);
    STAssertTrue([D isParamSubsetOf:F], nil);
    STAssertFalse([E isParamSubsetOf:F], nil);
}

-(void)testParamEquivalence {
    NSDictionary* A = @{};
    NSDictionary* B = @{@"1": @""};
    NSDictionary* C = @{@"1": @"", @"2": @""};
    NSDictionary* D = @{@"1": @"yes", @"2": @""};
    NSDictionary* E = @{@"1": @"no", @"2": @""};
    NSDictionary* F = @{@"1": @"yes", @"2": @"no"};
    
    STAssertTrue([A isParamEquivalent:A], nil);
    STAssertFalse([A isParamEquivalent:B], nil);
    STAssertFalse([B isParamEquivalent:A], nil);
    STAssertFalse([A isParamEquivalent:C], nil);
    STAssertFalse([B isParamEquivalent:C], nil);
    STAssertTrue([C isParamEquivalent:C], nil);
    STAssertFalse([C isParamEquivalent:D], nil);
    STAssertTrue([D isParamEquivalent:D], nil);
    STAssertFalse([D isParamEquivalent:E], nil);
    STAssertFalse([E isParamEquivalent:D], nil);
    STAssertTrue([F isParamEquivalent:F], nil);
}

@end
